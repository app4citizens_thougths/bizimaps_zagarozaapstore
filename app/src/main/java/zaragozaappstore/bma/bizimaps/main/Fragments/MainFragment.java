package zaragozaappstore.bma.bizimaps.main.Fragments;

import android.app.Activity;
import android.location.Address;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import duffman.libarary.gps.Tasks.Callbacks.DecodeLatLngCallback;
import duffman.libarary.gps.Tasks.DecodeLatLngTask;
import duffman.libarary.gps.LocationService;
import zaragozaappstore.bma.bizimaps.R;
import zaragozaappstore.bma.bizimaps.domainLayer.Callbacks.CallbackGetStations;
import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.User;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrDrawRoutes;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrGetStations;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrGetUser;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrShowStations;
import zaragozaappstore.bma.bizimaps.domainLayer.exceptions.NoMainUser;

/**
 * Created by pau on 03/07/15.
 */
public class MainFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener,CallbackGetStations {
    private final static String TAG="MainFragment";
    private User user;

    public interface CallbackMainFragment{
        void onMapClick(LatLng latLng);
        void onMarkerClick(Marker marker);
    }

    public static CallbackMainFragment voidCallback = new CallbackMainFragment() {

        @Override
        public void onMapClick(LatLng latLng) {

        }

        @Override
        public void onMarkerClick(Marker marker) {

        }
    };

    @Bind(R.id.map_view)
    MapView mapView;

    private LocationService map;
    private CallbackMainFragment callback=voidCallback;

    public static Fragment getInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this, view);

        mapView.onCreate(savedInstanceState);

        try {
            user = new TrGetUser(getActivity().getApplicationContext()).execute();
            if(user.isViewStations())
                new TrGetStations(this).execute();
        } catch (NoMainUser noMainUser) {
            noMainUser.printStackTrace();
        }



        LocationService.initialize(mapView.getMap(), getActivity().getApplicationContext());
        LocationService.addCallbackAndListener(this, this, this);
        LocationService.getLocationService().connect();
        LocationService.getLocationService().setOnMarkerClickListener(this);
        map= LocationService.getLocationService();

        boolean result=map.setCameraInYourPosition();
        if(!result)
            Toast.makeText(getActivity().getApplicationContext(),"No last position",Toast.LENGTH_SHORT).show();

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(!(activity instanceof CallbackMainFragment))
            throw new RuntimeException("Activity no implementa el callback");
        callback= (CallbackMainFragment) activity;

    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback=voidCallback;
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "onConected");

    }

    @Override
    public void onFinishedGetStations(List<Station> stations) {
        new TrShowStations().execute();
    }

    @Override
    public void onError(Exception retrofitError) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

        callback.onMapClick(latLng);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        callback.onMarkerClick(marker);
        return false;
    }
}
