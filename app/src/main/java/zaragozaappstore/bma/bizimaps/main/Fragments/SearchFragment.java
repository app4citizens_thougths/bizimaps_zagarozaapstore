package zaragozaappstore.bma.bizimaps.main.Fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.quinny898.library.persistentsearch.SearchBox;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import duffman.libarary.gps.DTOPlaceAutocomplete;
import duffman.libarary.gps.Tasks.AutocompleteTask;
import duffman.libarary.gps.Tasks.Callbacks.GetAutocompleteCallback;
import zaragozaappstore.bma.bizimaps.R;
import zaragozaappstore.bma.bizimaps.utils.Adapters.SearchAdapter;
import zaragozaappstore.bma.bizimaps.utils.Animation.FromTopAnimation;
import zaragozaappstore.bma.bizimaps.utils.Animation.ToTopAnimation;

/**
 * Created by pau on 05/07/15.
 */
public class SearchFragment extends Fragment implements SearchBox.SearchListener, GetAutocompleteCallback, SearchAdapter.SearchAdapterListener {



    public interface SearchFragmentCallback {
        SearchBox.MenuListener getMenuListener();
        void sendSearch(String query);
        void setSearchBox(SearchBox search);
        void onSearchOpened();
        void onSearchClosed();
        void onSearchCleared();
        void onItemSelected(DTOPlaceAutocomplete place);
    }

    private static SearchFragmentCallback voidCallback = new SearchFragmentCallback() {
        @Override
        public SearchBox.MenuListener getMenuListener() {
            return null;
        }
        @Override
        public void sendSearch(String query) {}
        @Override
        public void setSearchBox(SearchBox search) {}
        @Override
        public void onSearchOpened() {}
        @Override
        public void onSearchClosed() {}
        @Override
        public void onSearchCleared() {}

        @Override
        public void onItemSelected(DTOPlaceAutocomplete place) {

        }
    };

    private LinearLayoutManager mLayoutManager;
    private SearchAdapter mAdapter;

    @Bind(R.id.toolbar)
    SearchBox search;

    @Bind(R.id.background)
    FrameLayout backgroundLayout;

    @Bind(R.id.search_result_layout)
    LinearLayout resultLayout;

    @Bind(R.id.recycler)
    RecyclerView mRecyclerView;

    private SearchFragmentCallback callback= voidCallback;

    public static SearchFragment getInstance() {
        return new SearchFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment, container, false);
        ButterKnife.bind(this, view);

        search.enableVoiceRecognition(this);
        search.setLogoText("Buscar...");
        setMenuListener(callback.getMenuListener());
        search.setSearchListener(this);
        callback.setSearchBox(search);


        mRecyclerView.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);


        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if(!(activity instanceof SearchFragmentCallback))
            throw new RuntimeException("Activity no implementa el callback");
        callback= (SearchFragmentCallback) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback= voidCallback;
    }


    public void setMenuListener(SearchBox.MenuListener menuListener) {
        if(menuListener!=null)
            search.setMenuListener(menuListener);
    }

    @Override
    public void onSearchOpened() {
        callback.onSearchOpened();
        showSearchBackground();
    }

    @Override
    public void onSearchCleared() {
        callback.onSearchCleared();
        hideSearchBackground();
    }

    @Override
    public void onSearchClosed() {

    }

    private void showSearchBackground(){
        int color = getResources().getColor(R.color.search_background_color);
        backgroundLayout.setBackgroundColor(color);
        FromTopAnimation.setAnimation(getActivity(), backgroundLayout);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void hideSearchBackground(){
        //backgroundLayout.setBackgroundResource(0);
        ToTopAnimation.setAnimation(getActivity().getApplicationContext(), backgroundLayout);
        mRecyclerView.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onSearchTermChanged() {

    }

    @Override
    public void onSearch(String query) {
        callback.sendSearch(query);
        search.showLoading(true);
        new AutocompleteTask(getActivity().getApplicationContext(),query,this).execute();
    }

    @Override
    public void onGetAutocompleteFinish(ArrayList<DTOPlaceAutocomplete> dtoPlaceAutocompletes) {
        search.showLoading(false);
        if(dtoPlaceAutocompletes!=null) {
            String str = "{";

            for (DTOPlaceAutocomplete dto : dtoPlaceAutocompletes) {
                str += dto.toString() + ",";
            }
            str += "}";
            Log.d("MainActivity", "response: " + str);
        }

        mAdapter = new SearchAdapter(dtoPlaceAutocompletes,this);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onSearchItemClick(DTOPlaceAutocomplete place) {
        ToTopAnimation.setAnimation(getActivity().getApplicationContext(), backgroundLayout);
        mRecyclerView.setVisibility(View.INVISIBLE);
        callback.onItemSelected(place);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (isAdded() && requestCode == SearchBox.VOICE_RECOGNITION_CODE && resultCode == getActivity().RESULT_OK) {
            ArrayList<String> matches = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            ArrayList<String> result = new ArrayList<String>();
            result.add(matches.get(0));
            search.populateEditText(result);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}