package zaragozaappstore.bma.bizimaps.main;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeader;
import com.mikepenz.materialdrawer.accountswitcher.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SwitchDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IProfile;
import com.mikepenz.materialdrawer.model.interfaces.OnCheckedChangeListener;
import com.quinny898.library.persistentsearch.SearchBox;

import java.io.InputStream;
import java.net.URL;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import duffman.libarary.gps.DTOPlaceAutocomplete;
import duffman.libarary.gps.LocationService;
import duffman.libarary.gps.Tasks.Callbacks.DecodeLatLngCallback;
import duffman.libarary.gps.Tasks.DecodeLatLngTask;
import duffman.libarary.gps.Tasks.GetPlaceByIdTask;
import zaragozaappstore.bma.bizimaps.R;
import zaragozaappstore.bma.bizimaps.customViews.BottomLayout;
import zaragozaappstore.bma.bizimaps.domainLayer.Callbacks.CallbackGetStations;
import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.User;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrGetStationByMarker;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrGetStations;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrGetUser;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrHideStations;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrShowStations;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrUpdateUser;
import zaragozaappstore.bma.bizimaps.domainLayer.exceptions.NoMainUser;
import zaragozaappstore.bma.bizimaps.login.LoginActivity;
import zaragozaappstore.bma.bizimaps.main.Fragments.InfoStationFragment;
import zaragozaappstore.bma.bizimaps.main.Fragments.MainFragment;
import zaragozaappstore.bma.bizimaps.main.Fragments.SearchFragment;



public class MainActivity extends AppCompatActivity implements SearchBox.MenuListener,SearchFragment.SearchFragmentCallback,
        ResultCallback<PlaceBuffer>,MainFragment.CallbackMainFragment, DecodeLatLngCallback, CallbackGetStations {
    private Drawer drawer = null;

    private SearchBox search;
    private boolean searchIsOpened=false;
    private AccountHeader headerResult = null;
    private User user;
    private IProfile profile;

    @Bind(R.id.bottom_layout)
    BottomLayout bottomLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        try{
            loadUserData();
            createDrawer(savedInstanceState);
            setFragment(MainFragment.getInstance(), false);
            setSearchFragment();

        }catch(NoMainUser e){
            lauchLogin();
        }
    }

    private void loadUserData() throws NoMainUser {
        user = new TrGetUser(getApplicationContext()).execute();
    }

    private void lauchLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void createDrawer(Bundle savedInstanceState){
        profile = new ProfileDrawerItem().withName(user.getName()).withEmail(user.getEmail()).withIcon(LoadImageFromWebOperations(user.getPersonPhotoUrl()));

        // Create the AccountHeader
        buildHeader(false, savedInstanceState);
        drawer= new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult) //set the AccountHeader we created earlier for the header
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_navigate).withIcon(FontAwesome.Icon.faw_map_marker),
                        new SwitchDrawerItem().withName(R.string.drawer_item_enable_stations).withIcon(FontAwesome.Icon.faw_bicycle).withChecked(user.isViewStations()).withOnCheckedChangeListener(onCheckedChangeListenerStation),
                        new SwitchDrawerItem().withName(R.string.drawer_item_share_data).withIcon(FontAwesome.Icon.faw_database).withChecked(user.isShareData()).withOnCheckedChangeListener(onCheckedChangeListenerShare)
                )

                .withOnDrawerNavigationListener(new Drawer.OnDrawerNavigationListener() {
                    @Override
                    public boolean onNavigationClickListener(View clickedView) {
                        //this method is only called if the Arrow icon is shown. The hamburger is automatically managed by the MaterialDrawer
                        //if the back arrow is shown. close the activity
                        MainActivity.this.finish();
                        //return true if we have consumed the event
                        return true;
                    }

                }).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem) {
                        switch (position) {
                            case 0:
                                break;
                            case 1:
                                break;
                            default:
                                break;
                        }
                        return false;
                    }
                })
                .withAnimateDrawerItems(true)
                .withSavedInstance(savedInstanceState)
                .build();
    }
    /**
     * small helper method to reuse the logic to build the AccountHeader
     * this will be used to replace the header of the drawer with a compact/normal header
     *
     * @param compact
     * @param savedInstanceState
     */
    private void buildHeader(boolean compact, Bundle savedInstanceState) {
        // Create the AccountHeader
        headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.drawable.header)
                .withCompactStyle(compact)
                .addProfiles(
                        profile
                )
                .withSavedInstance(savedInstanceState)

                .build();
    }

    private OnCheckedChangeListener onCheckedChangeListenerStation= new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(IDrawerItem drawerItem, CompoundButton buttonView, boolean isChecked) {
            if (user.isViewStations()) {
                new TrHideStations().execute();
                user.setViewStations(false);
            }
            else {
                new TrGetStations(MainActivity.this).execute();
                user.setViewStations(true);
            }

            new TrUpdateUser(getApplicationContext(), user.isViewStations(), user.isShareData(), user.getEmail()).execute();
        }
    };

    private OnCheckedChangeListener onCheckedChangeListenerShare = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(IDrawerItem drawerItem, CompoundButton buttonView, boolean isChecked) {
            if (user.isShareData()) {
                user.setShareData(false);
            }
            else user.setShareData(true);
            new TrUpdateUser(getApplicationContext(), user.isViewStations(), user.isShareData(), user.getEmail()).execute();
        }
    };




    private Drawable LoadImageFromWebOperations(String url)
    {
        try
        {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        }catch (Exception e) {
            System.out.println("Exc="+e);
            return null;
        }
    }

    private void setFragment(Fragment fragment,boolean addToBackStack){
        setFragment(fragment,-1,-1,addToBackStack);
    }


    private void setFragment(Fragment fragment,int inAnim,int outAnim,boolean addToBackStack) {
        FragmentTransaction ft=getSupportFragmentManager().beginTransaction();
        if(addToBackStack)
            try {
                ft.addToBackStack(null);
            }catch (IllegalArgumentException e){
                e.printStackTrace();
            }

        if(inAnim!=-1 && outAnim!=-1)
            ft.setCustomAnimations(inAnim,inAnim,outAnim,outAnim);

        ft.replace(R.id.container,fragment);
        ft.commit();
    }

    public void setSearchFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_search, SearchFragment.getInstance());
        ft.commit();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onMenuClick() {
        if(drawer.isDrawerOpen())
            drawer.closeDrawer();
        else
            drawer.openDrawer();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(LocationService.hasInicialized())
            LocationService.getLocationService().connect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(LocationService.hasInicialized())
            LocationService.getLocationService().disconect();
    }

    @Override
    public SearchBox.MenuListener getMenuListener() {
        return this;
    }

    @Override
    public void sendSearch(String query) {
    }

    @Override
    public void setSearchBox(SearchBox search) {
        this.search=search;
    }

    @Override
    public void onSearchOpened() {
        searchIsOpened=true;
    }

    @Override
    public void onSearchClosed() {
        searchIsOpened=false;
    }

    @Override
    public void onSearchCleared() {
        searchIsOpened=true;
    }

    @Override
    public void onItemSelected(DTOPlaceAutocomplete place) {
        new GetPlaceByIdTask(getApplicationContext(),place,this).execute();

    }

    @Override
    public void onBackPressed() {
        Log.d("MainAcitivty","onBackPressed");
        if(search.getVisibility()==View.INVISIBLE) {
            search.setVisibility(View.VISIBLE);
            setFragment(MainFragment.getInstance(),false);
        }else if(bottomLayout.isShow()){
            bottomLayout.hide();
            LocationService.getLocationService().removeMainMarker();
        } else if(searchIsOpened) {
            search.clearResults();
            search.toggleSearch();
            searchIsOpened=false;
        }else
            finish();
    }

    @Override
    public void onResult(PlaceBuffer places) {
        String log ="places: {";

        if(places.getStatus().isSuccess()){
            for(Place place : places){
                log+="address: "+place.getAddress()+", lat: "+place.getLatLng().latitude+", lng: "+place.getLatLng().longitude;
            }
            Place place = places.get(0);
            LocationService.getLocationService().setMainMarker(place.getLatLng());
            new DecodeLatLngTask(getApplicationContext(),place.getLatLng(),this).execute();

        }else {
            String msg = getResources().getString(R.string.error_get_place_by_id);
            Toast.makeText(getApplicationContext(), msg,Toast.LENGTH_SHORT).show();
            log+=msg;
        }


        log+="}";
        Log.d("MainActivity", log);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        new DecodeLatLngTask(getApplicationContext(),latLng,this).execute();
    }

    @Override
    public void onMarkerClick(Marker marker) {

        final Station station = (new TrGetStationByMarker(marker)).execute();

        if(station!=null) {
            int disp = station.getBicisDisponibles();
            int ancl = station.getAnclajesDisponibles();

            String subtitle = getResources().getString(R.string.station_subtitle,disp,ancl);
            bottomLayout.setStation(station, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bottomLayout.hide();
                    search.setVisibility(View.INVISIBLE);
                    setFragment(InfoStationFragment.getInstance(station),
                            R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_bottom, false);
                }
            });
            showBottomLayout(station.getTitle(), subtitle, true, marker.getPosition());
        }
    }

    private void showBottomLayout(String title, String subtitle, boolean showInfo, LatLng latLngDest){
        if(!showInfo)
            bottomLayout.hideInfoButton();
        else bottomLayout.showInfoButton();
        Log.d("MainActivity","set for title: "+title);

        bottomLayout.setTitle(title);
        bottomLayout.setSubtitle(subtitle);
        if(!bottomLayout.isShow())
            bottomLayout.show();

        bottomLayout.setRouteDest(latLngDest);
    }

    @Override
    public void onTaskFinish(List<Address> result) {
        Address address = result.get(0);
        Log.d("MainActivity", "street: " + address.getAddressLine(0) + "; city: " + address.getLocality());
        showBottomLayout(address.getAddressLine(0),address.getLocality(),false,new LatLng(address.getLatitude(),address.getLongitude()));
    }



    @Override
    public void onFinishedGetStations(List<Station> stations) {
        new TrShowStations().execute();
    }

    @Override
    public void onError(Exception retrofitError) {

    }
}
