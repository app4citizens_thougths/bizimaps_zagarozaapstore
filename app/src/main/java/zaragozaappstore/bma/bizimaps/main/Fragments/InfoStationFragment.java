package zaragozaappstore.bma.bizimaps.main.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import zaragozaappstore.bma.bizimaps.R;
import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.StationService;

/**
 * Created by pau on 12/07/15.
 */
public class InfoStationFragment extends Fragment {
    private final static String ARG_ID_STATION="argIdStation";
    private Station station;
    private MapView mapView;
    private GoogleMap map;


    public static InfoStationFragment getInstance(Station station){
        InfoStationFragment fragment = new InfoStationFragment();
        Bundle args  = new Bundle();
        args.putString(ARG_ID_STATION, station.getId());
        fragment.setArguments(args);

        return fragment;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info_station, container, false);
        ButterKnife.bind(this, view);
        if(getArguments()!=null && getArguments().containsKey(ARG_ID_STATION)){
            station = StationService.getInstance().getStation(getArguments().getString(ARG_ID_STATION));
            TextView title = (TextView)view.findViewById(R.id.title);
            TextView disp = (TextView)view.findViewById(R.id.disp);
            TextView lib = (TextView)view.findViewById(R.id.lib);
            TextView last = (TextView)view.findViewById(R.id.last);

            //MAPS INICIALIZAE
            mapView = (MapView) view.findViewById(R.id.mapView);
            mapView.onCreate(savedInstanceState);

            map = mapView.getMap();
            map.getUiSettings().setMyLocationButtonEnabled(false);
            map.setMyLocationEnabled(true);

            MapsInitializer.initialize(this.getActivity());

            LatLng point = station.getLatLng();

            CameraUpdate cameraUpdate = CameraUpdateFactory.
                    newLatLngZoom(point, 16);

            map.clear();
            map.addMarker(new MarkerOptions()
                    .position(point)
                    .title(station.getTitle()));

            map.animateCamera(cameraUpdate);

            title.setText(station.getTitle());
            disp.setText(station.getBicisDisponibles()+"");
            lib.setText(station.getAnclajesDisponibles()+"");
            last.setText(station.getLastModified().toString());
        }

        return view;
    }
    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }


}
