package zaragozaappstore.bma.bizimaps.domainLayer.service.Async;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.Callbacks.CallbackGetStations;
import zaragozaappstore.bma.bizimaps.utils.UtilsInputStream;

/**
 * Created by pau on 11/07/15.
 */
public class GetStationsAsync extends AsyncTask<Void,Void,JSONObject> {
    private String url="https://iescities.com:443/IESCities/api/data/query/223/sql";
    private CallbackGetStations callback;

    public GetStationsAsync(CallbackGetStations callback) {
        this.callback = callback;
    }

    @Override
    protected JSONObject doInBackground(Void... params) {
        JSONObject response = null;
        try {
            String charset = "UTF-8";
            String param1 = "SELECT * FROM docs";
            //String param1 = "SELECT * FROM lugares;";

            URLConnection conn = new URL("https://iescities.com:443/IESCities/api/data/query/223/sql").openConnection();
            //conn.setReadTimeout(10000);
            //conn.setConnectTimeout(15000);
            //conn.setRequestMethod("POST");
            //conn.setDoInput(true);
            conn.setDoOutput(true);
            //conn.setDoOutput(false);
            conn.setRequestProperty("Accept-Charset", charset);
            conn.setRequestProperty("Content-Type", "text/plain");

            //final String basicAuth = "Basic " + Base64.encodeToString("bma:redbull".getBytes(), android.util.Base64.NO_WRAP);
            //conn.setRequestProperty("Authorization", basicAuth);

            OutputStream output = conn.getOutputStream();
            output.write(param1.getBytes());

            conn.connect();

            InputStream is = conn.getInputStream();

            response = new JSONObject(UtilsInputStream.getStringFromInputStream(is));

            is.close();


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        if(jsonObject!=null) {
            Log.d("GetStationsAsync", "result: " + jsonObject);
            try {
                JSONArray jsonArray = jsonObject.getJSONArray("rows");

                List<Station> stationList = new ArrayList<>();

                for(int i=0; i<jsonArray.length();++i) {
                    JSONObject object =jsonArray.getJSONObject(i);
                    Station station = new Station(object);
                    if(station.isOpen())
                        stationList.add(station);
                }

                callback.onFinishedGetStations(stationList);
            } catch (JSONException e) {
                e.printStackTrace();
                callback.onError(e);
            }
        }else {
            Log.e("GetStationsAsync", "result is null");
            callback.onError(new Exception("null result in GetStatinons query"));
        }
    }
}
