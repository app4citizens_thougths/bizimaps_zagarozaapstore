package zaragozaappstore.bma.bizimaps.domainLayer.controllers;


import android.content.Context;

import com.j256.ormlite.dao.Dao;

import zaragozaappstore.bma.bizimaps.domainLayer.User;
import zaragozaappstore.bma.bizimaps.utils.Sqlite.SqlBiziMaps;

/**
 * Created by davidgarciaruiz on 11/7/15.
 */
public class TrSaveUser {//
    private Context context;
    private String name;
    private String personPhotoUrl;
    private String email;


    public TrSaveUser(Context context, String name, String personPhotoUrl, String email){
        this.context = context;
        this.name = name;
        this.personPhotoUrl = personPhotoUrl;
        this.email = email;
    }
    public void execute() {
        Dao<User, String> accountDao = SqlBiziMaps.getInstance(context).getDaoByClass(User.class);

        User user = new User(name, personPhotoUrl, email);
        try {
            if (accountDao.create(user) != 1) {
                throw new RuntimeException();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    // close the connection source
    }
}
