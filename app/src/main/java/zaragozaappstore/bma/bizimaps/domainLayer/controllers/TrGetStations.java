package zaragozaappstore.bma.bizimaps.domainLayer.controllers;

import android.util.Log;

import java.util.List;

import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.StationService;
import zaragozaappstore.bma.bizimaps.domainLayer.service.BiziServiceAdapter;
import zaragozaappstore.bma.bizimaps.domainLayer.Callbacks.CallbackGetStations;

/**
 * Created by pau on 11/07/15.
 */
public class TrGetStations implements CallbackGetStations {


    private final CallbackGetStations callback;

    public TrGetStations(CallbackGetStations callback){
        this.callback = callback;
    }

    public void execute(){
        new BiziServiceAdapter().getStations(this);
    }

    @Override
    public void onFinishedGetStations(List<Station> stations) {
        Log.d("TRGetStations", "size: " + stations.size());
        StationService.getInstance().addAllStations(stations);
        callback.onFinishedGetStations(stations);
    }

    @Override
    public void onError(Exception error) {
        callback.onError(error);
    }
}
