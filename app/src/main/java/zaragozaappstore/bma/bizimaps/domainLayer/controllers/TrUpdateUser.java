package zaragozaappstore.bma.bizimaps.domainLayer.controllers;

import android.content.Context;

import com.j256.ormlite.dao.Dao;

import zaragozaappstore.bma.bizimaps.domainLayer.User;
import zaragozaappstore.bma.bizimaps.utils.Sqlite.SqlBiziMaps;

/**
 * Created by davidgarciaruiz on 11/7/15.
 */
public class TrUpdateUser {
    private final boolean isStation;
    private final boolean isShared;
    private final String email;
    private Context context;


    public TrUpdateUser(Context context, boolean isStation, boolean isShared, String email){
        this.context = context;
        this.isStation = isStation;
        this.isShared = isShared;
        this.email = email;
    }
    public void execute() {
        Dao<User, String> accountDao = SqlBiziMaps.getInstance(context).getDaoByClass(User.class);
        User user = null;
        try {
            user = accountDao.queryForId(email);
        } catch (Exception e) {
            e.printStackTrace();
        }
        user.setShareData(isShared);
        user.setViewStations(isStation);
        try {
            accountDao.update(user);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        // close the connection source
    }
}
