package zaragozaappstore.bma.bizimaps.domainLayer.controllers;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import duffman.libarary.gps.LocationService;
import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.StationService;

/**
 * Created by pau on 11/07/15.
 */
public class TrShowStations {

    public void execute(){

        List<Station> stationList = StationService.getInstance().getAll();

        for(Station station: stationList){
            MarkerOptions markerOptions= new MarkerOptions()
                    .position(station.getLatLng())
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));

            Marker marker=LocationService.getLocationService().addMarker(markerOptions);
            station.setMarker(marker);

        }
    }
}
