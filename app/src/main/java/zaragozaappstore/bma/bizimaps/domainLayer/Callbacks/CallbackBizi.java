package zaragozaappstore.bma.bizimaps.domainLayer.Callbacks;

/**
 * Created by davidgarciaruiz on 11/7/15.
 */
public interface CallbackBizi {
    void onError(Exception retrofitError);
}