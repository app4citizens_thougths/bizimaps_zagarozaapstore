package zaragozaappstore.bma.bizimaps.domainLayer;

import android.net.Uri;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;

import zaragozaappstore.bma.bizimaps.utils.Date;

/**
 * Created by pau on 11/07/15.
 */
public class Station {
    private String id;
    private String title;



    private boolean state;
    private int bicisDisponibles;
    private int anclajesDisponibles;
    private LatLng latLng;
    private double xCoordinate;
    private double yCoordinate;
    private double coordenadasP0Coordinate;



    private double coordenadasP1Coordinate;
    private String text;
    private Date lastModified;
    private String description;
    private String language;
    private URL iconUrl;
    private Marker marker;

    public Station (JSONObject jsonObject) throws JSONException {
        setId(jsonObject.getString("id"));
        setTitle(jsonObject.getString("title"));
        setBicisDisponibles(jsonObject.getInt("bicisdisponibles_i"));
        setAnclajesDisponibles(jsonObject.getInt("anclajesdisponibles_i"));

        String strLatLng = jsonObject.getString("coordenadas_p");
        String[] splitLatLng= strLatLng.split(",");
        setLatLng(new LatLng(Double.parseDouble(splitLatLng[0]), Double.parseDouble(splitLatLng[1])));

        setxCoordinate(jsonObject.getDouble("x_coordinate"));
        setyCoordinate(jsonObject.getDouble("y_coordinate"));
        setCoordenadasP0Coordinate(jsonObject.getDouble("coordenadas_p_0_coordinate"));
        setCoordenadasP1Coordinate(jsonObject.getDouble("coordenadas_p_1_coordinate"));

        String strState = jsonObject.getString("estado_t");
        setState(strState.equals("OPN"));

        setText(jsonObject.getString("texto_t"));
        setLastModified(Date.getApiDate(jsonObject.getString("last_modified")));
        setDescription(jsonObject.getString("description"));
        setLanguage(jsonObject.getString("language"));

        try {
            setIconUrl(new URL(jsonObject.getString("icon_t")));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getBicisDisponibles() {
        return bicisDisponibles;
    }

    public void setBicisDisponibles(int bicisDisponibles) {
        this.bicisDisponibles = bicisDisponibles;
    }

    public int getAnclajesDisponibles() {
        return anclajesDisponibles;
    }

    public void setAnclajesDisponibles(int anclajesDisponibles) {
        this.anclajesDisponibles = anclajesDisponibles;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public double getxCoordinate() {
        return xCoordinate;
    }

    public void setxCoordinate(double xCoordinate) {
        this.xCoordinate = xCoordinate;
    }

    public double getyCoordinate() {
        return yCoordinate;
    }

    public void setyCoordinate(double yCoordinate) {
        this.yCoordinate = yCoordinate;
    }

    public double getCoordenadasP1Coordinate() {
        return coordenadasP1Coordinate;
    }

    public void setCoordenadasP1Coordinate(double coordenadasP1Coordinate) {
        this.coordenadasP1Coordinate = coordenadasP1Coordinate;
    }

    public double getCoordenadasP0Coordinate() {
        return coordenadasP0Coordinate;
    }

    public void setCoordenadasP0Coordinate(double coordenadasP0Coordinate) {
        this.coordenadasP0Coordinate = coordenadasP0Coordinate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public URL getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(URL iconUrl) {
        this.iconUrl = iconUrl;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Station) && ((Station) o ).getId().equals(getId());
    }

    @Override
    public String toString() {
        return "id: "+getId()+", title: "+getTitle()+", latLng: "+getLatLng();
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public void removeMarker() {
        marker.remove();
        marker=null;
    }

    public Marker getMarker() {
        return marker;
    }

    public boolean haveBicisDisponibles(){
        return bicisDisponibles>0;
    }

    public boolean haveAnclajesDisponibles(){
        return anclajesDisponibles>0;
    }

    public boolean isOpen() {
        return state;
    }

    public void setState(boolean state) {
        this.state = state;
    }
}
