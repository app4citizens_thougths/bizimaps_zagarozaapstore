package zaragozaappstore.bma.bizimaps.domainLayer;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import duffman.libarary.gps.LocationService;
import zaragozaappstore.bma.bizimaps.R;
import zaragozaappstore.bma.bizimaps.domainLayer.service.BiziServiceAdapter;
import zaragozaappstore.bma.bizimaps.utils.Date;

/**
 * Created by pau on 12/07/15.
 */
public class TraceRouteService implements LocationListener{
    private static final long MEASURE_TIME = 5;
    private static TraceRouteService traceRouteService=null;
    public static final double MIN_DIST = 10.0;
    private final Context context;
    private final LocationManager lm;
    private Polyline polyline;
    private Date startDate;
    private Date endDate;

    public static TraceRouteService getInstance(Context context){
        if(traceRouteService==null) traceRouteService = new TraceRouteService(context);
        return traceRouteService;
    }

    public static double getDist (LatLng start, LatLng arrival) {
        double lat1 = start.latitude;
        double lng1 = start.longitude;
        double lat2 = arrival.latitude;
        double lng2 =arrival.longitude;
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = earthRadius * c;

        int meterConversion = 1609;
        double result = dist * meterConversion;
        return result;

    }


    private Station startStation;
    private Station endStation;
    private boolean inRoute=false,finishedRoute=false;
    private List<RoutePoint> routePointList = new ArrayList<>();
    private double meters;


    private TraceRouteService (Context context){
        this.context = context;

        lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);



    }

    public boolean startRoute(Station startStation, Station endStation){
        Log.d("TraceRouteService","startRoute");
        inRoute=false;
        finishedRoute=false;
        routePointList= new ArrayList<>();
        meters=0;
        this.startStation = startStation;
        this.endStation = endStation;

        if(polyline!=null)
            polyline.remove();
        PolylineOptions polylineOptions = new PolylineOptions().color(Color.BLUE).width(10);
        polyline=LocationService.getLocationService().addPolyline(polylineOptions);

        startMesures();
        return true;
    }

    public Station getStartStation() {
        return startStation;
    }

    public void setStartStation(Station startStation) {
        this.startStation = startStation;
    }

    public Station getEndStation() {
        return endStation;
    }

    public void setEndStation(Station endStation) {
        this.endStation = endStation;
    }

    public boolean finishRoute(){
        if(!inRoute) return false;
        inRoute=false;
        finishedRoute=true;
        endMesures();
        return true;
    }

    public boolean stayInRoute(){
        return inRoute;
    }

    public double getMeters(){
        return meters;
    }

    public boolean routeFinished(){ return finishedRoute;}

    public boolean addPointToRoute(RoutePoint routePoint){
        if(!inRoute) return false;
        if(!routePointList.isEmpty())
            meters+=getDist(routePointList.get(routePointList.size()-1).getLatLng(),routePoint.getLatLng());

        routePointList.add(routePoint);
        return true;
    }

    public List<RoutePoint> getRoutePoints(){ return routePointList;}

    public void startMesures(){
        Log.d("TraceRoute","startMesures");
        lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, MEASURE_TIME, 10, this);
        //lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MEASURE_TIME, 10, this);
    }

    public void endMesures(){
        lm.removeUpdates(this);
    }


    @Override
    public void onLocationChanged(Location location) {

        double distStartStation = getDist(new LatLng(location.getLatitude(), location.getLongitude()), startStation.getLatLng());
        double distEndStation = getDist(new LatLng(location.getLatitude(), location.getLongitude()), endStation.getLatLng());
        if(!inRoute && distStartStation<MIN_DIST){
            inRoute=true;
            startDate= new Date();
            String msg = context.getResources().getString(R.string.start_routing_msg);
            Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
        }else if (inRoute && !finishedRoute && distEndStation<MIN_DIST){
            String msg = context.getResources().getString(R.string.end_routing_msg);
            endDate=new Date();
            Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
            new BiziServiceAdapter().insertRoute(startStation,endStation,startDate,endDate,meters,routePointList);
            finishRoute();
        }

        if(inRoute && !finishedRoute){
            RoutePoint routePoint = new RoutePoint(location);
            addPointToRoute(routePoint);

            List<LatLng> points = polyline.getPoints();
            points.add(routePoint.getLatLng());
            polyline.setPoints(points);
        }

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(context,"Conexión",Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(context,"Error en el proveidor de GPS",Toast.LENGTH_SHORT).show();

    }
}
