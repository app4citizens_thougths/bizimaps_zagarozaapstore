package zaragozaappstore.bma.bizimaps.domainLayer.controllers;

import java.util.List;

import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.StationService;

/**
 * Created by pau on 11/07/15.
 */
public class TrHideStations {

    public TrHideStations(){

    }

    public void execute(){
        List<Station> stationList = StationService.getInstance().getAll();
        for(Station station: stationList) station.removeMarker();
    }
}
