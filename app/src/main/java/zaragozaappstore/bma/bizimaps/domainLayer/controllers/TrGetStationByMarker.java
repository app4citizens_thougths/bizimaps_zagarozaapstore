package zaragozaappstore.bma.bizimaps.domainLayer.controllers;

import com.google.android.gms.maps.model.Marker;

import java.util.List;

import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.StationService;

/**
 * Created by pau on 12/07/15.
 */
public class TrGetStationByMarker {

    private final Marker marker;

    public TrGetStationByMarker(Marker marker){
        this.marker = marker;
    }

    public Station execute(){
        List<Station> stationList = StationService.getInstance().getAll();

        for(Station station : stationList)
            if(station.getMarker()!=null && station.getMarker().equals(marker))
                return station;

        return null;
    }
}
