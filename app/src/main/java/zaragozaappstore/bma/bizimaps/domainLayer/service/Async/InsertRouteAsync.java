package zaragozaappstore.bma.bizimaps.domainLayer.service.Async;

import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import zaragozaappstore.bma.bizimaps.domainLayer.RoutePoint;
import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.utils.Date;
import zaragozaappstore.bma.bizimaps.utils.UtilsInputStream;

/**
 * Created by pau on 12/07/15.
 */
public class InsertRouteAsync extends AsyncTask<Void,Void,JSONObject> {
    private final Station startStation;
    private final Station endStation;
    private final List<RoutePoint> points;
    private final Date startDate;
    private final Date endDate;
    private final double meters;

    public InsertRouteAsync(Station startStation, Station endStation,Date startDate, Date endDate,double meters,List<RoutePoint> points){
        this.startStation = startStation;
        this.endStation = endStation;
        this.points = points;
        this.startDate = startDate;
        this.endDate = endDate;
        this.meters = meters;
    }
    @Override
    protected JSONObject doInBackground(Void... params) {
        JSONObject response = null;
        try {
            int mainId= 1234 +(int) (Math.random() * (12756786 - 1234));
            String charset = "UTF-8";
            String query = "INSERT INTO BiziRoutes (id,startStation,endStation,meters,dateIni,dateSend) " +
                    "VALUES ("+mainId+",'"+startStation.getId()+"','"+endStation.getId()+"',"+meters+",'"
                    +startDate.getBiziString()+"','"+endDate.getBiziString()+"');";

            for(RoutePoint point : points){
                int routeId= 12234 +(int) (Math.random() * (435542545 - 12234));
                query+="INSERT INTO BiziRoutes_routes (_id,latitude,longitude,date,parent_id) " +
                        "VALUES ("+routeId+","+point.getLatLng().latitude+","+point.getLatLng().longitude+",'"
                        +point.getDate().getBiziString()+"',"+mainId+");";
            }
            Log.d("InsertRouteAsync", "query: "+query);

            URLConnection conn = new URL("https://iescities.com:443/IESCities/api/data/update/316/sql?transaction=true").openConnection();
            //conn.setReadTimeout(10000);
            //conn.setConnectTimeout(15000);
            //conn.setRequestMethod("POST");
            //conn.setDoInput(true);
            conn.setDoOutput(true);
            //conn.setDoOutput(false);
            conn.setRequestProperty("Accept-Charset", charset);
            conn.setRequestProperty("Content-Type", "text/plain");

            final String basicAuth = "Basic " + Base64.encodeToString("bma:redbull".getBytes(), android.util.Base64.NO_WRAP);
            conn.setRequestProperty("Authorization", basicAuth);

            OutputStream output = conn.getOutputStream();
            output.write(query.getBytes());

            conn.connect();

            InputStream is = conn.getInputStream();

            response = UtilsInputStream.getJSONObjectFromInputStream(is);

            is.close();


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return response;
    }

    @Override
    protected void onPostExecute(JSONObject object) {
        super.onPostExecute(object);
    }
}
