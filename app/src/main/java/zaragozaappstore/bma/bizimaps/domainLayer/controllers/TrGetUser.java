package zaragozaappstore.bma.bizimaps.domainLayer.controllers;

import android.content.Context;
import android.content.SharedPreferences;

import com.j256.ormlite.dao.Dao;

import zaragozaappstore.bma.bizimaps.domainLayer.User;
import zaragozaappstore.bma.bizimaps.domainLayer.exceptions.NoMainUser;
import zaragozaappstore.bma.bizimaps.utils.Sqlite.SqlBiziMaps;


/**
 * Created by davidgarciaruiz on 11/7/15.
 */

public class TrGetUser {

    private Context context;
    public static final String MY_PREFS = "LoginPreference";

    public TrGetUser(Context context) {
        this.context = context;

    }

    public User execute() throws NoMainUser {

        Dao<User, String> accountDao = SqlBiziMaps.getInstance(context).getDaoByClass(User.class);

        User account = null;
        SharedPreferences prefs = context.getSharedPreferences(MY_PREFS, context.MODE_PRIVATE);
        String email = prefs.getString("email", null);
        if (email != null) {
            try {
                account = accountDao.queryForId(email);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else throw new NoMainUser();

        return account;
    }
}
