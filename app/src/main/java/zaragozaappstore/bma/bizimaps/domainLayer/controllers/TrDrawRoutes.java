package zaragozaappstore.bma.bizimaps.domainLayer.controllers;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.util.Log;

import com.directions.route.Route;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.List;

import duffman.libarary.gps.LocationService;
import zaragozaappstore.bma.bizimaps.domainLayer.Callbacks.CallbackGetStations;
import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.TraceRouteService;
import zaragozaappstore.bma.bizimaps.domainLayer.User;
import zaragozaappstore.bma.bizimaps.domainLayer.exceptions.NoMainUser;

/**
 * Created by davidgarciaruiz on 11/7/15.
 */
public class TrDrawRoutes implements CallbackGetStations{
    private final Context context;
    private LatLng myLatLang;
    private LatLng myArrival;
    private Station startStation;
    private Station endStation;

    public double distFrom (LatLng start, LatLng arrival) {
        double lat1 = start.latitude;
        double lng1 = start.longitude;
        double lat2 = arrival.latitude;
        double lng2 =arrival.longitude;
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = earthRadius * c;

        int meterConversion = 1609;
        double result = dist * meterConversion;
        return result;

    }

    /*public TrDrawRoutes() {
        Location loc = LocationService.getLocationService().getUserLocation();
        myLatLang = new LatLng(loc.getLatitude(),loc.getLongitude());
        myArrival = LocationService.getLocationService().getMainMarkerLocation();
        LocationService.getLocationService().cleanPolylines();
        LocationService.getLocationService().cleanCircularMarker();
    }*/

    public TrDrawRoutes(Context context,LatLng arrival) {
        this.context = context;
        Location loc = LocationService.getLocationService().getUserLocation();
        myLatLang = new LatLng(loc.getLatitude(),loc.getLongitude());
        myArrival = arrival;

        LocationService.getLocationService().cleanCircularMarker();
        LocationService.getLocationService().cleanPolylines();
    }


    public void execute(){
        new TrGetStations(this).execute();
    }

    public int getClosestInitStation(List<Station> stations, LatLng goal) {
        int minIndex = -1;
        double minDist = 1E38; // initialize with a huge value that will be overwritten
        int size = stations.size();
        for (int i = 0; i < size; i++) {
            Station station = stations.get(i);
            double curDistance = distFrom(station.getLatLng(), goal);
            if (station.haveBicisDisponibles() &&  curDistance < minDist) {
                minDist = curDistance;  // update neares
                minIndex = i;           // store index of nearest marker in minIndex
            }
        }
        return minIndex;
    }

    public int getClosestEndStation(List<Station> stations, LatLng goal) {
        int minIndex = -1;
        double minDist = 1E38; // initialize with a huge value that will be overwritten
        int size = stations.size();
        for (int i = 0; i < size; i++) {
            Station station = stations.get(i);
            double curDistance = distFrom(station.getLatLng(), goal);
            if (station.haveAnclajesDisponibles() &&  curDistance < minDist) {
                minDist = curDistance;  // update neares
                minIndex = i;           // store index of nearest marker in minIndex
            }
        }
        return minIndex;
    }

    @Override
    public void onError(Exception retrofitError) {

    }

    @Override
    public void onFinishedGetStations(List<Station> stations) {
        int minIndexStart = getClosestInitStation(stations, myLatLang);
        int minIndexFinish = getClosestEndStation(stations, myArrival);
        startStation = stations.get(minIndexStart);
        endStation = stations.get(minIndexFinish);
        drawLines(myLatLang, startStation.getLatLng());
        drawMidLine(startStation.getLatLng(), endStation.getLatLng());
        drawLines(endStation.getLatLng(), myArrival);

        try {
            User user = (new TrGetUser(context)).execute();

            if(user.isShareData())
                TraceRouteService.getInstance(context).startRoute(startStation, endStation);
        } catch (NoMainUser noMainUser) {}
    }

    public void drawLines(LatLng start, LatLng finish){

        Routing routing = new Routing(Routing.TravelMode.WALKING);
        routing.registerListener(new WalkRoutingListener());
        routing.execute(start, finish);

    }

    public void drawMidLine(LatLng start, LatLng finish){
        Routing bike = new Routing(Routing.TravelMode.DRIVING);
        bike.registerListener(new BikeRoutingListener());
        bike.execute(start, finish);
    }


    private abstract class BasicRoutingListener implements RoutingListener{
        @Override
        public void onRoutingSuccess(PolylineOptions polylineOptions, Route route) {
            polylineOptions.color(getColor());
            polylineOptions.width(10);
            LocationService.getLocationService().addPolyline(polylineOptions);
            onFinish(route);
        }

        @Override
        public void onRoutingFailure() {

        }

        @Override
        public void onRoutingStart() {

        }

        public abstract int getColor();
        public abstract void onFinish(Route route);
    }

    private class WalkRoutingListener extends BasicRoutingListener{
        @Override
        public int getColor() {
            Log.d("WalkRoutingListener","gre");
            return Color.GRAY;
        }

        @Override
        public void onFinish(Route route) { }
    }

    private class BikeRoutingListener extends BasicRoutingListener{
        @Override
        public int getColor() {
            Log.d("WalkRoutingListener","red");
            return Color.RED;
        }

        @Override
        public void onFinish(Route route) {
            MarkerOptions markerOptions = new MarkerOptions().position(route.getPoints().get(0));
            LocationService.getLocationService().addCircularMarker(markerOptions);

            markerOptions = new MarkerOptions().position(route.getPoints().get(route.getPoints().size()-1));
            LocationService.getLocationService().addCircularMarker(markerOptions);
        }
    }
}
