package zaragozaappstore.bma.bizimaps.domainLayer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pau on 11/07/15.
 */
public class StationService {
    private static StationService stationService;
    public static StationService getInstance(){
        if(stationService==null) stationService= new StationService();
        return stationService;
    }

    private List<Station> stations;

    private StationService (){
        stations= new ArrayList<>();
    }


    public boolean addStation(Station station){
        if(stations.contains(station)) return false;

        stations.add(station);
        return true;
    }

    public boolean removeStation(Station station){
        return stations.remove(station);
    }

    public List<Station> getAll(){
        return stations;
    }

    public Station getStation(String id){
        for(int i = 0; i< stations.size();++i){
            String title = stations.get(i).getId().toString();
            if(title.equals(id)){
                return stations.get(i);
            }
        }
        return null;
    }

    public void addAllStations(List<Station> stations) {
        List<Station> aux = new ArrayList<>(this.stations);
        this.stations= new ArrayList<>();

        for(Station station :stations){
            int index = aux.indexOf(station);

            if(index!=-1)
                station.setMarker(aux.get(index).getMarker());

            this.stations.add(station);

        }
    }
}
