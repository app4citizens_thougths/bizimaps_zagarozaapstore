package zaragozaappstore.bma.bizimaps.domainLayer.Callbacks;

import java.util.List;

import zaragozaappstore.bma.bizimaps.domainLayer.Station;

/**
 * Created by davidgarciaruiz on 11/7/15.
 */
public interface CallbackGetStations extends CallbackBizi{
    void onFinishedGetStations(List<Station> stations);
}