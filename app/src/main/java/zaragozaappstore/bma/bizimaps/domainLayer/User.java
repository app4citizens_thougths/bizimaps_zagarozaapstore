package zaragozaappstore.bma.bizimaps.domainLayer;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


/**
 * Created by davidgarciaruiz on 11/7/15.
 */
@DatabaseTable(tableName = "User")
public class User {
    @DatabaseField(id = true)
    String email;
    @DatabaseField(canBeNull = false)
    String name;
    @DatabaseField(canBeNull = false)
    String personPhotoUrl;
    @DatabaseField(canBeNull = false)
    boolean shareData;
    @DatabaseField(canBeNull = false)
    boolean viewStations;
    User() {
        // all persisted classes must define a no-arg constructor
        // with at least package visibility
    }

    public User(String name, String personPhotoUrl, String email){
        this.name = name;
        this.personPhotoUrl = personPhotoUrl;
        this. email = email;
        this.viewStations = true;
        this.shareData = false;
    }



    public boolean isShareData() {
        return shareData;
    }

    public void setShareData(boolean shareData) {
        this.shareData = shareData;
    }

    public boolean isViewStations() {
        return viewStations;
    }

    public void setViewStations(boolean viewStations) {
        this.viewStations = viewStations;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonPhotoUrl() {
        return personPhotoUrl;
    }

    public void setPersonPhotoUrl(String personPhotoUrl) {
        this.personPhotoUrl = personPhotoUrl;
    }
}
