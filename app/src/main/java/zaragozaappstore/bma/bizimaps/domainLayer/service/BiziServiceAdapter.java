package zaragozaappstore.bma.bizimaps.domainLayer.service;

/**
 * Created by davidgarciaruiz on 11/7/15.
 */


import java.util.List;

import zaragozaappstore.bma.bizimaps.domainLayer.RoutePoint;
import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.service.Async.GetStationsAsync;
import zaragozaappstore.bma.bizimaps.domainLayer.Callbacks.CallbackGetStations;
import zaragozaappstore.bma.bizimaps.domainLayer.service.Async.InsertRouteAsync;
import zaragozaappstore.bma.bizimaps.utils.Date;

public class BiziServiceAdapter {
    private final int DATABASE = 223;
    private final String USER = "bma";
    private final String PASSWORD = "redbull";
    private final String ID = "id_question";
    private final String VALUE = "value";
    private final String YES = "1";
    private final String NO = "0";
    private final String LATITUDE = "latitude";
    private final String LONGITUDE = "longitude";
    private final String QUESTION = "question";
    private final int RESPONSE_OK = 200;

    private final static String ALL_STATIONS_QUERY="SELECT * FROM docs";

    public void getStations(final CallbackGetStations callback){
        new GetStationsAsync(callback).execute();
    }

    public void insertRoute(Station startStation, Station endStation,Date startDate, Date endDate,double meters,List<RoutePoint> points){
        new InsertRouteAsync(startStation,endStation,startDate,endDate,meters,points).execute();
    }
}