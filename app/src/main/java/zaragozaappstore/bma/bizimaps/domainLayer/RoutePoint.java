package zaragozaappstore.bma.bizimaps.domainLayer;

import android.location.Location;

import com.directions.route.Route;
import com.google.android.gms.maps.model.LatLng;

import zaragozaappstore.bma.bizimaps.utils.Date;

/**
 * Created by pau on 12/07/15.
 */
public class RoutePoint {

    private LatLng position;
    private Date date;

    public RoutePoint(LatLng position){
        this.position=position;
        date= new Date();
    }

    public RoutePoint(Location userLocation) {
        this(new LatLng(userLocation.getLatitude(),userLocation.getLongitude()));
    }

    public LatLng getLatLng() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "lat: "+position.latitude+", lng: "+position.longitude;
    }
}
