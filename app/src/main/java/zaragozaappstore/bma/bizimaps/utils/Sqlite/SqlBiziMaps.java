package zaragozaappstore.bma.bizimaps.utils.Sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import zaragozaappstore.bma.bizimaps.domainLayer.User;

public class SqlBiziMaps extends OrmLiteSqliteOpenHelper {
    static private SqlBiziMaps sqlBiziMaps = null;

    static public  SqlBiziMaps getInstance(Context context){
        if(sqlBiziMaps==null)
            sqlBiziMaps= new SqlBiziMaps( context,name,factory, version);

        return sqlBiziMaps;
    }

    private static String name="bizi_maps";
    private static int version=1;
    private static SQLiteDatabase.CursorFactory factory = null;

    private SqlBiziMaps(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connection) {
        try {
            TableUtils.createTable(connection, User.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
    }

    public Dao getDaoByClass(Class T){
        try {
            return getDao(T);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
