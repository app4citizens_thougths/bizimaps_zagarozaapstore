package zaragozaappstore.bma.bizimaps.utils.Animation;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import zaragozaappstore.bma.bizimaps.R;

/**
 * Created by pau on 06/07/15.
 */
public abstract class ToTopAnimation {
    public static void setAnimation(Context context, FrameLayout layout){
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.to_top);
        layout.startAnimation(myFadeInAnimation);
    }
}
