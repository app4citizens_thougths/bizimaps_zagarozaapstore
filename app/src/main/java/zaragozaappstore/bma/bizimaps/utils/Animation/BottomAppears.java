package zaragozaappstore.bma.bizimaps.utils.Animation;

import android.content.Context;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import zaragozaappstore.bma.bizimaps.R;

/**
 * Created by pau on 12/07/15.
 */
public class BottomAppears {
    public static void setAnimation(Context context, FrameLayout layout){
        Animation myFadeInAnimation = AnimationUtils.loadAnimation(context, R.anim.abc_slide_in_bottom);
        layout.startAnimation(myFadeInAnimation);
    }
}
