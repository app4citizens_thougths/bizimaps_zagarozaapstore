package zaragozaappstore.bma.bizimaps.utils.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

/**
 * Created by pau on 06/07/15.
 */
public abstract class CustomAdapter<S,T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {


    private final int layoutId;
    private final List<S> data;

    public CustomAdapter(int layoutId,List<S> data){
        this.layoutId = layoutId;
        this.data = data;
    }

    @Override
    public T onCreateViewHolder(ViewGroup parent, int i) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(layoutId, parent, false);

        T vh = getInstance(v);
        return vh;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public S getItem(int i) {
        return data.get(i);
    }

    public abstract T getInstance(View view);
}
