package zaragozaappstore.bma.bizimaps.utils.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import duffman.libarary.gps.DTOPlaceAutocomplete;
import zaragozaappstore.bma.bizimaps.R;

/**
 * Created by pau on 06/07/15.
 */
public class SearchAdapter extends CustomAdapter<DTOPlaceAutocomplete,SearchAdapter.ViewHolder>{


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public LinearLayout mainLayout;

        public ViewHolder(View view) {
            super(view);
            mTextView = (TextView) view.findViewById(R.id.tv_description);
            mainLayout = (LinearLayout) view.findViewById(R.id.main_layout_item);
        }
    }

    public interface SearchAdapterListener {
        void onSearchItemClick(DTOPlaceAutocomplete item);
    }

    private final SearchAdapterListener listener;

    public SearchAdapter(List<DTOPlaceAutocomplete> data,SearchAdapterListener listener) {
        super(R.layout.search_item_view, data);
        this.listener = listener;
    }

    @Override
    public ViewHolder getInstance(View view) {
        ViewHolder viewHolder= new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        viewHolder.mTextView.setText(getItem(i).description);

        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onSearchItemClick(getItem(i));
            }
        });
    }





}
