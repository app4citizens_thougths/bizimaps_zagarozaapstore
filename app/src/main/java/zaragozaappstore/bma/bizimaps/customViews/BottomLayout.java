package zaragozaappstore.bma.bizimaps.customViews;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.software.shell.fab.FloatingActionButton;

import zaragozaappstore.bma.bizimaps.R;
import zaragozaappstore.bma.bizimaps.domainLayer.Station;
import zaragozaappstore.bma.bizimaps.domainLayer.controllers.TrDrawRoutes;
import zaragozaappstore.bma.bizimaps.utils.Animation.BottomAppears;
import zaragozaappstore.bma.bizimaps.utils.Animation.BottomHide;

/**
 * Created by pau on 11/07/15.
 */
public class BottomLayout  extends FrameLayout implements View.OnClickListener {
    private TextView title,subtitle;
    private FloatingActionButton routeButton,infoButton;
    private FrameLayout mainLayout;
    private boolean isShow;
    private LatLng routeDest;
    private Station station;
    private boolean haveData=false;

    public BottomLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.bottom_layout, this);

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.BottomLayout,
                0, 0);

        title= (TextView) findViewById(R.id.title);
        subtitle = (TextView) findViewById(R.id.subtitle);
        routeButton =(FloatingActionButton) findViewById(R.id.get_route_button);
        infoButton = (FloatingActionButton) findViewById(R.id.more_info_button);
        mainLayout=(FrameLayout) findViewById(R.id.main_layout);

        isShow = a.getBoolean(R.styleable.BottomLayout_show_layout,true);
        if(!isShow) mainLayout.setVisibility(View.INVISIBLE);

        boolean showInfoButton = a.getBoolean(R.styleable.BottomLayout_show_info_button,true);
        if(!showInfoButton)
            infoButton.setVisibility(View.INVISIBLE);
    }

    public void setTitle(String title){
        haveData=true;
        this.title.setText(title);
    }

    public void setSubtitle(String subtitle){
        this.subtitle.setText(subtitle);
    }

    public void hide(){
        isShow=false;
        mainLayout.setVisibility(View.INVISIBLE);
        BottomHide.setAnimation(getContext(), this);
    }

    public void hideInfoButton(){
        infoButton.setVisibility(View.INVISIBLE);
    }

    public void showInfoButton(){
        infoButton.setVisibility(View.VISIBLE);

    }

    public boolean isShowInfoButton(){
        return infoButton.getVisibility()==View.VISIBLE;
    }

    public void show(){
        isShow=true;
        if(mainLayout.getVisibility()==View.INVISIBLE)
            mainLayout.setVisibility(View.VISIBLE);
        BottomAppears.setAnimation(getContext(), this);
    }

    public boolean isShow(){
        return isShow;
    }

    public void setRouteDest(LatLng routeDest) {
        this.routeDest = routeDest;
        routeButton.setOnClickListener(this);
    }

    public void setStation(Station station,OnClickListener infoClickListener){
        this.station = station;
        this.infoButton.setOnClickListener(infoClickListener);
    }

    @Override
    public void onClick(View v) {
        new TrDrawRoutes(getContext(),routeDest).execute();
    }

    public boolean haveData() {
        return haveData;
    }

    public String getTitle() {
        return title.getText().toString();
    }

    public String getSubtitle() {
        return subtitle.getText().toString();
    }

    public LatLng getLatLng() {
        return station.getLatLng();
    }

    public Station getStation() {
        return station;
    }
}
