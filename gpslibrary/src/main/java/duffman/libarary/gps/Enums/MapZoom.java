package duffman.libarary.gps.Enums;

/**
 * Created by pau on 04/07/15.
 */
public enum MapZoom {
    FAR(6),
    STANDARD(14),
    NEAR(19);


    private int value;

    MapZoom(int value){
        this.value=value;
    }

    public int getValue(){ return value;}
}
