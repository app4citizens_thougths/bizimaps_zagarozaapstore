package duffman.libarary.gps.Tasks.Callbacks;

import android.location.Address;

import java.util.List;

/**
 * Created by pau on 11/07/15.
 */
public interface DecodeLatLngCallback{
    void onTaskFinish(List<Address> result);
}
