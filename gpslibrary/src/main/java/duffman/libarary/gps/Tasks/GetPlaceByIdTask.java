package duffman.libarary.gps.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import duffman.libarary.gps.DTOPlaceAutocomplete;
import duffman.libarary.gps.LocationService;

/**
 * Created by pau on 11/07/15.
 */
public class GetPlaceByIdTask{

    private final Context context;
    private final DTOPlaceAutocomplete place;
    private final ResultCallback<PlaceBuffer> callback;
    private final GoogleApiClient mGoogleApiClient;

    public GetPlaceByIdTask(Context context,DTOPlaceAutocomplete place,ResultCallback<PlaceBuffer> callback){
        this.context = context;
        this.place = place;
        this.callback = callback;
        mGoogleApiClient= LocationService.getLocationService().getApiClient();
    }

    public void execute() {
        Log.d("GetPlaceByIdTask", "placeId: " + place.placeId);
        Places.GeoDataApi.getPlaceById(mGoogleApiClient, place.placeId)
                .setResultCallback(callback);
    }
}
