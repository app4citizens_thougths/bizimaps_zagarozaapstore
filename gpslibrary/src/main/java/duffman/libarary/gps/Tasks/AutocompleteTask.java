package duffman.libarary.gps.Tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.AutocompletePredictionBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import duffman.libarary.gps.DTOPlaceAutocomplete;
import duffman.libarary.gps.Tasks.Callbacks.GetAutocompleteCallback;
import duffman.libarary.gps.LocationService;

/**
 * Created by pau on 05/07/15.
 */
public class AutocompleteTask extends AsyncTask<Void, Void, ArrayList<DTOPlaceAutocomplete>> {

    private final Context context;
    private final GetAutocompleteCallback callback;
    private final String query;
    private final GoogleApiClient mGoogleApiClient;

    public AutocompleteTask(Context context,String query,GetAutocompleteCallback callback){
        this.context = context;
        this.query = query;
        this.callback = callback;
        mGoogleApiClient= LocationService.getLocationService().getApiClient();
    }
    @Override
    protected ArrayList<DTOPlaceAutocomplete> doInBackground(Void... params) {
        LatLng southWest = new LatLng(41.611409,-0.955212);
        LatLng northEast= new LatLng(41.705802,-0.825779);

        LatLngBounds bounds = new LatLngBounds(southWest,northEast);
        AutocompleteFilter filter = AutocompleteFilter.create(null);


        Log.d("LocationService","start search...");
        PendingResult<AutocompletePredictionBuffer> results= Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, query,
                bounds, filter);

        // This method should have been called off the main UI thread. Block and wait for at most 60s
        // for a result from the API.
        AutocompletePredictionBuffer autocompletePredictions = results
                .await(60, TimeUnit.SECONDS);

        // Confirm that the query completed successfully, otherwise return null
        final com.google.android.gms.common.api.Status status = autocompletePredictions.getStatus();
        if (!status.isSuccess()) {
            Log.e("LocationService", "Error getting autocomplete prediction API call: " + status.toString());
            autocompletePredictions.release();
            return null;
        }

        Log.i("LocationService", "Query completed. Received " + autocompletePredictions.getCount()
                + " predictions.");

        // Copy the results into our own data structure, because we can't hold onto the buffer.
        // AutocompletePrediction objects encapsulate the API response (place ID and description).

        Iterator<AutocompletePrediction> iterator = autocompletePredictions.iterator();
        ArrayList resultList = new ArrayList<>(autocompletePredictions.getCount());
        while (iterator.hasNext()) {
            AutocompletePrediction prediction = iterator.next();
            // Get the details of this prediction and copy it into a new PlaceAutocomplete object.
            resultList.add(new DTOPlaceAutocomplete(prediction.getPlaceId(),
                    prediction.getDescription()));
        }

        // Release the buffer now that all data has been copied.
        autocompletePredictions.release();

        return resultList;
    }

    @Override
    protected void onPostExecute(ArrayList<DTOPlaceAutocomplete> dtoPlaceAutocompletes) {
        super.onPostExecute(dtoPlaceAutocompletes);
        callback.onGetAutocompleteFinish(dtoPlaceAutocompletes);
    }
}
