package duffman.libarary.gps.Tasks.Callbacks;

import java.util.ArrayList;

import duffman.libarary.gps.DTOPlaceAutocomplete;

/**
 * Created by pau on 05/07/15.
 */
public interface GetAutocompleteCallback {
    void onGetAutocompleteFinish(ArrayList<DTOPlaceAutocomplete> dtoPlaceAutocompletes);
}
