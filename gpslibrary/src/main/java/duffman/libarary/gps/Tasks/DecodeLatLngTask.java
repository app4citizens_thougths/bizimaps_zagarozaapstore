package duffman.libarary.gps.Tasks;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import duffman.libarary.gps.Tasks.Callbacks.DecodeLatLngCallback;

/**
 * Created by pau on 07/07/15.
 */
public class DecodeLatLngTask extends AsyncTask<Void,Void,List<Address>> {
    private final static int MAX_RESULTS=10;



    private final double latitude;
    private final double longitude;
    private final int max;
    private Context context;
    private DecodeLatLngCallback callback;

    public DecodeLatLngTask(Context context,LatLng latLng,DecodeLatLngCallback callback){
        this(context,latLng.latitude,latLng.longitude,MAX_RESULTS,callback);
    }

    public DecodeLatLngTask(Context context, double latitude, double longitude, int max, DecodeLatLngCallback callback){
        this.latitude = latitude;
        this.longitude = longitude;
        this.max = max;
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected List<Address> doInBackground(Void... params) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, max);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return addresses;
    }

    @Override
    protected void onPostExecute(List<Address> address) {
        super.onPostExecute(address);
        callback.onTaskFinish(address);
    }
}
