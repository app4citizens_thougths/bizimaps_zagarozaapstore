package duffman.libarary.gps;

/**
 * Created by pau on 05/07/15.
 */
public class DTOPlaceAutocomplete {
    public final String placeId;
    public final String description;

    public DTOPlaceAutocomplete(String placeId, String description) {
        this.placeId = placeId;
        this.description = description;
    }

    @Override
    public String toString() {
        return "placeId: "+placeId+"; description: "+description;
    }
}
